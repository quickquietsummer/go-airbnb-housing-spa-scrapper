package main

import (
	"context"
	"fmt"
	"github.com/chromedp/chromedp"
	"log"
	"strconv"
	"time"
)

var url = "https://www.airbnb.ru/"

type housing struct {
	country   string
	place     string
	dateRange string
	price     string
}

var selectorFormat = "#site-content > div.fowfzmz.dir.dir-ltr > div > div > div > div > div > div:nth-child(%s) > div > div.c1l1h97y.dir.dir-ltr > div > div > div > div.cy5jw6o.dir.dir-ltr > div > div.g1qv1ctd.cb4nyux.dir.dir-ltr"

func main() {
	ctx, cancel := chromedp.NewContext(context.Background())
	defer cancel()
	fmt.Println("-----Context created")
	housings := make([]housing, 0)
	housings = append(housings, produceHousing(ctx, 2))
	fmt.Println("-----housing appended")

	fmt.Println(housings)
	fmt.Println("END")
}

func getSelectorById(id int) string {
	return fmt.Sprintf(selectorFormat, strconv.Itoa(id))
}

func produceHousing(ctx context.Context, id int) housing {
	selectorRoot := getSelectorById(id)
	selectorCountry := fmt.Sprintf("%s > div:nth-child(1)", selectorRoot)
	selectorPlace := fmt.Sprintf("%s > div:nth-child(2)", selectorRoot)
	selectorDateRange := fmt.Sprintf("%s > div:nth-child(3)", selectorRoot)
	selectorPrice := fmt.Sprintf("%s > div:nth-child(4)", selectorRoot)

	var country string
	var place string
	var dateRange string
	var price string
	err := chromedp.Run(
		ctx,
		chromedp.Navigate(url),
		RunWithTimeOut(&ctx, 3, chromedp.Tasks{
			chromedp.WaitVisible(getSelectorById(id), chromedp.ByQuery),
			chromedp.Text(selectorCountry, &country, chromedp.ByQuery),
			chromedp.Text(selectorPlace, &place, chromedp.ByQuery),
			chromedp.Text(selectorDateRange, &dateRange, chromedp.ByQuery),
			chromedp.Text(selectorPrice, &price, chromedp.ByQuery),
		}),
	)

	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("-----no error in housing producing")

	return housing{
		country,
		place,
		dateRange,
		price,
	}
}
func RunWithTimeOut(ctx *context.Context, timeout time.Duration, tasks chromedp.Tasks) chromedp.ActionFunc {
	return func(ctx context.Context) error {
		timeoutContext, cancel := context.WithTimeout(ctx, timeout*time.Second)
		defer cancel()
		return tasks.Do(timeoutContext)
	}
}
